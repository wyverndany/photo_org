#!/usr/bin/python
##############################################################################
#
# By Daniel Rodriguez
# Programa para Ana 
# Lee los archivos en el folder proporcionado y crea un archivo con los detalles
#
##############################################################################

import datetime
from PIL import Image
from PIL.ExifTags import TAGS
import os
import sys

def get_date_taken(ph_fl):
    img = Image.open(ph_fl)
    exif = img._getexif()
    if exif is None:
        return None
    for k, v in exif.iteritems():
        if TAGS.get(k) == 'DateTimeOriginal':
            return v
        
# img = Image.open('test.JPG')
# exif = {
#     TAGS[k]: v
#     for k, v in img._getexif().items()
#     if k in TAGS
# }    
# for e in exif:
#     print e, exif[e]

def make_path(orig,new):
    ''' receives a complete path to a file and returns the original path with the file renamed'''
    # get the path without the file name
    pt_end = orig.rfind("/")
    pt = orig[:pt_end+1]
    
    # get the extension
    ext_end= orig.rfind(".")
    ext = orig[ext_end:]
    
    name = pt + new + ext
#     print "%s => %s" %(orig, name)
    return name

if __name__ == "__main__":

    ''' Script to read a directory recursively and rename photos with the date taken\n
    Only argument needed is the folder'''
    # Check that the input is correct
    # you need a folder to look at and that it exists\
    if len(sys.argv) != 2:
        print
        print 'You need to input the folder to crawl through'
        print ' '
        sys.exit()
    else:
        if os.path.isdir(sys.argv[1]):
            print ' '
            print ' Will scan the directory:    ',sys.argv[1]
            print ' '
        else:
            print ' '
            print ' Directory does not exist'
            print ' '
            sys.exit()
    
    d = sys.argv[1]
    
    files = []
    for dirpath, directories, fl in os.walk(d):
        for each in fl:
            files.append(dirpath+'/'+each)

    unk = []
    non_image = []
    for i, f in enumerate(files):
#         print f
        dt = None
        try:
            dt = get_date_taken(f)
        except:
            non_image.append(f)
        if dt is None:
            d_info = "Unkown"
            unk.append(f)
            continue
        # convert the string date obtained into wanted format
        try:
            d_info = datetime.datetime.strptime(dt,"%Y:%m:%d %H:%M:%S").strftime('%Y_%m_%d__%H_%M_%S')
            nn = make_path(f, d_info)
            os.rename(f,nn)
        except:
            d_info = "Unkown"
            unk.append(f)
    #     print "  ", i+1, d_info
    
    print "Summary : %i files processed:" %len(files)
    print "   of which %i is/are not images:" %len(non_image)
    for e in non_image:
        print "      -", e
    print "   %i files do not have info on the date taken" %(len(unk))
    print "   They were not renamed..."
    for e in unk:
        print "      -", e